import imp
from xml.dom.minidom import Document
from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
   path('',views.home, name = 'home'),
#    path('create/',views.create, name='create'),
#    path('results/<poll_id>/',views.results,name='results'),
#    path('vote/<poll_id>/',views.vote,name='vote'),
#    path('record',views.record,name='record'),
#    path('',views.pagination,name='home'),
   ]

if settings.DEBUG:
    urlpatterns+=static(settings.MEDIA_URL,Document_root=settings.MEDIA_ROOT)