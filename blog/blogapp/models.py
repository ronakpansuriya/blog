from django.db import models
from django.contrib.auth.models import User

# Create your models here.
# class Category(models.Model):
#     blog_type = models.CharField(max_length=30,null=True)

#     def __str__(self):
#         return self.blog_type

# class Blog(models.Model):
#     bloguser = models.ForeignKey(User, on_delete=models.CASCADE)
#     # blogcategory = models.OneToOneField(Category, on_delete= models.CASCADE )
#     blogtitle = models.CharField(max_length=20)
#     blogimage = models.FileField(upload_to='blogapp/images',default="")
#     blogdecription = models.TextField()

#     def __str__(self):
#         return self.blogtitle

class Post(models.Model):
    post_user = models.ForeignKey(User, on_delete=models.CASCADE)
    post_title = models.CharField(max_length=20)
#     blogimage = models.FileField(upload_to='blogapp/images',default="")
    post_decription = models.TextField()
    
    def __str__(self):
        return self.post_title